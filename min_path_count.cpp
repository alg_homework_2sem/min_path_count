/*
 * Задача 3. Колличество различных путей.
 * Дан невзвешенный неориентированный граф. В графе может быть несколько
 * кратчайших путей между какими-то вершинами. Найдите количество различных
 * кратчайших путей между заданными вершинами. Требуемая сложность O(V+E).
 * Память - O(V)
 */
#include <iostream>
#include <list>
#include <queue>
#include <vector>

using std::list;
using std::queue;
using std::vector;

struct IGraph {
  virtual ~IGraph() {}

  virtual void AddEdge(int from, int to) = 0;

  virtual int VerticesCount() const = 0;

  virtual void GetNextVertices(int vertex,
                               std::vector<int>& vertices) const = 0;
  virtual void GetPrevVertices(int vertex,
                               std::vector<int>& vertices) const = 0;
};

struct ListGraph : public IGraph {
 private:
  std::vector<std::list<int> > adjacency_list;
  std::vector<std::list<short> > reverse_adjacency_list;

 public:
  ListGraph(int n) : adjacency_list(n), reverse_adjacency_list(n) {}
  ~ListGraph() = default;
  ListGraph(const IGraph* graph);
  void AddEdge(int from, int to) override;
  int VerticesCount() const override;
  void GetNextVertices(int vertex, std::vector<int>& vertices) const override;
  void GetPrevVertices(int vertex, std::vector<int>& vertices) const override;
};

int getMinPathCount(const IGraph& graph, int start, int end);

int main() {
  int vertices, edges;
  std::cin >> vertices >> edges;

  ListGraph graph(vertices);

  for (int i = 0; i < edges; i++) {
    int start, end;
    std::cin >> start >> end;
    graph.AddEdge(start, end);
    graph.AddEdge(end, start);
  }
  int start, end;
  std::cin >> start >> end;

  std::cout << getMinPathCount(graph, start, end);

  return 0;
}

int getMinPathCount(const IGraph& graph, int start, int end) {
  size_t verticesCount = graph.VerticesCount();
  vector<int> distances(verticesCount, -1);
  vector<int> pathCount(verticesCount);  // Cчитаем динамику.
  queue<int> currentQueue;
  currentQueue.push(start);
  pathCount[start] = 1;  // Начальные значения.
  distances[start] = 0;
  vector<int> nextVertices;
  while (!currentQueue.empty()) {
    size_t curVertex = currentQueue.front();
    currentQueue.pop();
    graph.GetNextVertices(curVertex, nextVertices);
    for (auto neigh : nextVertices) {
      if (distances[neigh] == -1) {
        distances[neigh] = distances[curVertex] + 1;
        pathCount[neigh] += pathCount[curVertex];
        currentQueue.push(neigh);
      } else if (distances[neigh] == distances[curVertex] + 1) {
        pathCount[neigh] += pathCount[curVertex];  // Пересчет
      }
    }
  }
  return pathCount[end];
}
void ListGraph::AddEdge(int from, int to) {
  ListGraph::adjacency_list[from].push_front(to);
  //  ListGraph::reverse_adjacency_list[to].push_front(from); Иначе вылетает ML)
}

int ListGraph::VerticesCount() const {
  return ListGraph::adjacency_list.size();
}

void ListGraph::GetNextVertices(int vertex, vector<int>& vertices) const {
  vertices.clear();
  for (auto it : ListGraph::adjacency_list[vertex]) {
    vertices.push_back(it);
  }
}

void ListGraph::GetPrevVertices(int vertex, vector<int>& vertices) const {
  for (auto it : ListGraph::reverse_adjacency_list[vertex]) {
    vertices.push_back(it);
  }
}

ListGraph::ListGraph(const IGraph* graph)
    : adjacency_list(graph->VerticesCount()) {
  vector<int> temp;
  for (int i = 0; i < adjacency_list.size(); i++) {
    temp.clear();
    graph->GetNextVertices(i, temp);
    adjacency_list[i].insert(adjacency_list[i].end(), temp.begin(), temp.end());
  }
}
